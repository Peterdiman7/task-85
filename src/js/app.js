import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready
  const ul = document.querySelector("ul");

  let pokemonArr = [];
  const catchEmAll = () => {
    pokemonArr = [];
      fetch("https://pokeapi.co/api/v2/pokemon/?limit=10")
        .then(res => res.json())
        .then(poke => poke.results.forEach(v => {
          let pokeLi = document.createElement('li');
          pokeLi.textContent = v.name;
          ul.appendChild(pokeLi);
        }));
      }

      catchEmAll();
});
